FROM ubuntu:20.04

WORKDIR /app

COPY . .

RUN apt-get -y update 
RUN apt-get -y install software-properties-common
	
RUN ln -s /usr/bin/python3.8 /usr/bin/python

RUN apt-get -y install python3.8-distutils
RUN python get-pip.py

RUN python -m pip install fastapi
RUN python -m pip install uvicorn
RUN python -m pip install aiofiles

CMD [ "uvicorn", "--host=0.0.0.0", "--port=8000", "main:app" ]
EXPOSE 8000

# docker build -t panorama .
# docker image ls panorama
# docker run -p 80:8000 -d panorama